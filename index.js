require("dotenv").config();

const { google } = require("googleapis");
const sheets = google.sheets("v4");

const SERVICE_ACCOUNT_KEY_FILE = process.env.SERVICE_ACCOUNT_KEY_FILE;
const SPREADSHEET_ID = process.env.SPREADSHEET_ID;

const onRequest = (req, res) => {
  const payload = req.body;

  console.log(req.body);
  console.log(req.method);
  console.log(req.get("content-type"));

  const { event } = req.body;
  const { user, text } = event;
  console.log(event);
  console.log(text);

  const regex = /(http(s)?:\/\/([\w-]+\.)+[\w-]+(\/[\w-.\/?%&=]*)?)|(#([^\s]+))/gi;
  const result = text.match(regex);
  let tag = [];
  let http = [];

  if (regex.test(text)) {
    result.forEach(element => {
      element.startsWith("#")
        ? tag.push(element.substring(1))
        : http.push(element);
    });

    const jwtClient = new google.auth.JWT(
      null,
      SERVICE_ACCOUNT_KEY_FILE,
      null,
      ["https://www.googleapis.com/auth/spreadsheets"],
      null
    );

    return new Promise((resolve, reject) => {
      jwtClient.authorize((err, tokens) => {
        if (err) {
          reject(err);
        } else {
          google.options({
            auth: jwtClient
          });
          resolve();

          // row 개수 확인
          sheets.spreadsheets.values.get(
            {
              spreadsheetId: SPREADSHEET_ID,
              range: "sheet1"
            },
            (err, result) => {
              if (err) {
                console.log(err);
              } else {
                let values = [[http[0], "MESAAGE", tag[0], "USER ID", "TIME"]];
                const resource = {
                  values
                };
                sheets.spreadsheets.values.update(
                  {
                    spreadsheetId: SPREADSHEET_ID,
                    range: "A" + (result.data.values.length + 1),
                    valueInputOption: "RAW",
                    resource: resource
                  },
                  (err, result) => {
                    if (err) {
                      // Handle error
                      console.log(err);
                    } else {
                      console.log("%d cells updated.", result.updatedCells);
                    }
                  }
                );
              }
            }
          );

          res.sendStatus(200);
        }
      });
    });
  }

  res.sendStatus(200);
};

exports.slackPinboard = onRequest;
